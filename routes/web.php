<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Response;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @create date 2022-03-14 15:16:27
 * @modify date 2022-03-14 15:16:27
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Redirect all unhandled routes
 */
Route::fallback(function () {
    $responseData = new \stdClass;
    $responseData->statusCode = Response::HTTP_NOT_FOUND;
    $responseData->data = ['statusCode' => Response::HTTP_NOT_FOUND, 'error' => 'Not Found', 'message' => 'Oh noes, there is nothing in here'];
    return jsonErrorResponse($responseData);
});
