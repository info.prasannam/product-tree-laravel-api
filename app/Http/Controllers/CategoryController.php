<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CategoryService;

/**
 * @author [prasanna]
 * @email [info.prasannam@gmail.com]
 * @create date 2022-03-14 14:59:24
 * @modify date 2022-03-14 14:59:24
 */

class CategoryController extends Controller
{
    /**
     * Services implementation.
     *
     * @var CategoryService
     */
    protected $categoryService;

    /**
     * Create a new controller instance.
     *
     * @param  CategoryService  $categoryService
     * @return void
     */
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id = null)
    {
        return $this->categoryService->index($request, $id);
    }
}
